const fs = require("fs");
const PDFDocument = require("pdfkit");

function createInvoice(invoice, path) {
  let doc = new PDFDocument({ margin: 50 });

  generateHeader(doc);
  generateCustomerInformation(doc, invoice);
  generateInvoiceTable(doc, invoice);
  generateFooter(doc);

  doc.end();
  doc.pipe(fs.createWriteStream(path));
}


function generateInvoiceTable(doc, invoice) {
    let i,
      invoiceTableTop = 260;
  
    for (i = 0; i < invoice.items.length; i++) {
      const item = invoice.items[i];
      const position = invoiceTableTop + (i + 1)*20;
      generateTableRow(
        doc,
        position,
        item.item,
        item.description,
        item.amount / item.quantity,
        item.quantity,
        item.amount
      );
    }
  }


function generateTableRow(doc, y, c1, c2, c3, c4, c5) {
    doc
      .fontSize(8)
      .text(c1, 50, y)
      .text(c2, 150, y)
      .text(c3, 280, y, { width: 90, align: "right" })
      .text(c4, 370, y, { width: 90, align: "right" })
      .text(c5, 0, y, { align: "right" });
  }
function generateCustomerInformation(doc, invoice) {
    const shipping = invoice.shipping;
  
    doc
      .text(`Numero de commande: ${invoice.invoice_nr}`, 50, 210)
      .text(`Date de commande: ${new Date()}`, 50, 225)
      .text(`Montant total : ${invoice.subtotal - invoice.paid}`, 50, 130)
  
      .text("M./Mme: "+shipping.name, 50, 180)
      .text("Contact: "+shipping.phone, 50, 190)
      .text("Email: "+shipping.email, 50, 200)


      .text("Adresse de livraison: "+shipping.address, 50, 235)

      .text("Produits",50,255)
      .text("Description",150,255)
      .text("Prix unitair",350,255)
      .text("Qts",450,255)
      .text("Sous total",520,255)
      .moveDown();
  }


function generateHeader(doc) {
    doc
    .image("./public/images/c.png", 50, 45, { width: 100 ,height:50})
      .fillColor("#444444")
      .fontSize(8)
      .text("Aladin Technologies SARL.", 60, 100)
      .fontSize(8)
      .text("Yopougon, Ananeraie antene", 200, 65, { align: "right" })
      .text("+225 0546326368", 100, 80, { align: "right" })
       
      .moveDown();
  }
  
  function generateFooter(doc) {
    doc
    .image("./public/images/c.png",175,700,{width:50,height:50},{align: "center"}
    )
      .fontSize(10)
      .text(
        "Plus que trois voeux!!",
        50,
        730,
        { align: "center", width: 500 }
      );
  }


const invoice = {
    shipping: {
      name: "David kouame",
      address: "Yopougon, Ananeraie antene",
      city: "Abidjan",
      country: "COTE D'IVOIRE",
      phone:"0546326368",
      email:"kdbestonly@gmail.com"
    },
    items: [
      {
        item: "TC 100",
        description: "Toner Cartridge",
        quantity: 2,
        amount: 6000
      },
      {
        item: "USB_EXT",
        description: "USB Cable Extender",
        quantity: 1,
        amount: 2000
      }
    ],
    subtotal: 8000,
    paid: 0,
    invoice_nr: 1234
  };

  createInvoice(invoice,"public/invoice.pdf")
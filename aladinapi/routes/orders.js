    var express = require('express');
    var db = require('../config/db');
    const sendmail=require('../config/mail/notify');
    var ordermail = require('../config/ordermail')
    const order_date=require('../config/order_date');
    var router =express.Router();

/**
 * get all pending orders
*/
router.get('/pending',(req,res)=>{
    var sql =`SELECT aladin_orders.ord_id,aladin_orders.status,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_orders.dmode,aladin_orders.created_at ,aladin_product_ordered.description, aladin_orders.total,aladin_orders.city,aladin_orders.d_place FROM aladin_orders
    INNER  JOIN aladin_users ON aladin_orders.customer=aladin_users.user_id
     INNER JOIN aladin_product_ordered ON aladin_product_ordered.orders=aladin_orders.ord_id
     WHERE status like ?`;
    db.query(sql,['%'+ "pending" + '%'],(err,resul)=>{
        if(err){
            throw err
        
        }else
        {
            res.send({
                 status:true,
                 data:resul

            }
            );
        }
    });
})


router.put('/ok/:id',(req,res)=>{
    let data=[[req.body.status]]
    let sql =`UPDATE aladin_orders SET status=? WHERE ord_id=${req.params.id}`;
    db.query(sql,[data],(err,result)=>{
        if(err){
            res.send(
                {
                    status:false,
                    message:err.sqlMessage,
                    err:error
                }
            )
        }
        res.send({
            status:true,
            message:"commande mise en mode de production",
            res:result
        })
    })

})

router.put('/pending/:id',(req,res)=>{
    let data=[[req.body.status]]
    let sql =`UPDATE aladin_orders SET status=? WHERE ord_id=${req.params.id}`;
    db.query(sql,[data],(err,result)=>{
        if(err){
            res.send(
                {
                    status:false,
                    message:err.sqlMessage,
                    err:error
                }
            )
        }
        res.send({
            status:true,
            message:"commande mise en mode de production",
            res:result
        })
    })

})

/***
 * get all delivered orders
*/
router.get('/ok',(req,res)=>{
    var sql  =`SELECT aladin_orders.ord_id,aladin_orders.status,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_orders.dmode,aladin_orders.created_at ,aladin_product_ordered.description, aladin_orders.total,aladin_orders.city,aladin_orders.d_place FROM aladin_orders
     INNER  JOIN aladin_users ON aladin_orders.customer=aladin_users.user_id
     INNER JOIN aladin_product_ordered ON aladin_product_ordered.orders=aladin_orders.ord_id
     WHERE status like ?`;
    db.query(sql,['%'+"ok"+'%'],(err,r)=>{
        if (err)throw err;
        
        res.send({
            data:r,
            status:true
        });
    });
});


/**
 * 
 * 
*/
router.put('/aladin/orders/rejected/:id',(req,res)=>{
    let data=[[req.body.status]]
    let sql =`UPDATE aladin_orders SET status=? WHERE ord_id=${req.params.id}`;
    db.query(sql,[data],(err,result)=>{
        if(err){
            res.status(400).send(
                {
                    status:false,
                    message:err.sqlMessage,
                    err:error
                }
            )
        }
        res.status(200).send({
            status:true,
            message:"commande mise en mode de production",
            res:result
        })
    })

})
router.get('/aladin/orders/rejected',(req,res)=>{
    let sql=`SELECT aladin_orders.ord_id,aladin_orders.status,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_orders.dmode,aladin_orders.created_at ,aladin_product_ordered.description, aladin_orders.total,aladin_orders.city,aladin_orders.d_place FROM aladin_orders
   INNER  JOIN aladin_users ON aladin_orders.customer=aladin_users.user_id
    INNER JOIN aladin_product_ordered ON aladin_product_ordered.orders=aladin_orders.ord_id
    WHERE status like ?
    `;
    db.query(sql,['%'+"rejected",'%'],(err,result)=>{
        if(err){
            res.send(
                {
                error:err,
                status:false,
                message:"erreur mauvaise requete"
                })
        }
        res.send({
            data:result,
            status:true,
            message:"succès"
            
        }
    )

    })
})


router.get('/aladin/orders',(req,res)=>{
    let sql=`SELECT aladin_orders.ord_id,aladin_orders.status,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_orders.dmode,aladin_orders.created_at,aladin_product_ordered.description, aladin_orders.total,aladin_orders.city,aladin_orders.d_place FROM aladin_orders
    INNER  JOIN aladin_users ON aladin_orders.customer=aladin_users.user_id
    INNER JOIN aladin_product_ordered ON aladin_product_ordered.orders=aladin_orders.ord_id
    WHERE status like ?
    `;
    db.query(sql,['%'+"new",'%'],(err,result)=>{
        if(err){
            res.send(
                {
                error:err,
                status:false,
                message:"erreur mauvaise requete"
                })
        }  
        res.status(200).send({
            data:result,
            status:true,
            message:"succès"
            
        }
    )

    })
})


router.post('/',function(req, res, next){
        var sql = "INSERT INTO aladin_orders(status,customer,created_at,dmode,d_place,total,city) VALUES ?";
        var user=parseInt(req.body.customer);
        var created_at=order_date();
        var value=[
            [req.body.status,user,created_at,req.body.dmode,req.body.d_place,req.body.total,req.body.city]
        ];
        db.query(sql,[value],(err,result)=>{
            if(err){
                res.status(400).send(
                    {
                        error:err,
                        status:false,
                        code:400

                    }
                    )
            }
                res.status(201).send({
                message:"commande ajoutée",
                resp:result,
                status:true,
                code:201
            });

        });
    });

router.post("/products",(req,res)=>{
    let tab = JSON.parse(req.body.items)
    let value=[]
    for(let item of tab){
        value.push([JSON.stringify(item),item.id,req.body.order])
    }

    var sql="INSERT INTO aladin_product_ordered(description,cloth,orders) VALUES ?";
    if(value.length>0){

    db.query(sql,[value],(error,resp)=>{
        if(error){
            res.status.send(400).send(
                {
                    status:false,
                    code:400,
                    error:error,
                }
            )
        }
        let emaildata={
            name:req.body.name
        }
        let message={
            to:req.body.email,
            subject:"Aladin",
            html:ordermail(emaildata)
          };
          sendmail(message.to,message.subject,message.html);
        res.status(201).send(
            {
                status:true,
                code:201,
                data:resp
            }
        )
    })
    }

})
 
router.get('/customers/:id',(req,res)=>{
        let user= req.params.id;
        let sql=`SELECT aladin_orders.ord_id,aladin_orders.status,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_orders.dmode,aladin_orders.created_at,aladin_product_ordered.description, aladin_orders.total,aladin_orders.city,aladin_orders.d_place FROM aladin_orders
        INNER  JOIN aladin_users ON aladin_orders.customer=aladin_users.user_id
        INNER JOIN aladin_product_ordered ON aladin_product_ordered.orders=aladin_orders.ord_id
        WHERE aladin_orders.customer=${user}
        `;
        db.query(sql,(err,result)=>{
            if(err){
              res.status(400).send(
                  {
                    status:false,
                    error:err
                  }
              )
            }

            res.status(200).send(
                {
                    status:true,
                    data:result,
                    message:"reçu"
                }
            )
           
        });    
    });






 module.exports = router;

var express = require('express');
var router = express.Router();
var db = require('../config/db')
const sendmail=require('../config/mail/notify')

/* Post uploaded file. */

router.post('/', function(req, res, next){
    const file = req.files.img
    console.log(file,req.body);
    file.mv("public/gadgets/"+file.name,(err,resp)=>{
       if(err){
        throw err;
       }else{
            var sql = "INSERT INTO aladin_gadgets(name_gadget,price,dim,size,diam,material,img,user_id,comment) VALUES ?";
            var user=parseInt(req.body.user_id);
            var value=[
                [req.body.name_gadget,req.body.price,req.body.dim,req.body.size,req.body.diam,req.body.material,"https://api.aladin.ci/gadgets/"+file.name,user,req.body.comment]
            ];
            db.query(sql,[value],(err,result)=>{
                if(err){
                    throw err
                }else{
                res.send({
                    message:"Vetement ajouté",
                    resp:result
                });
                }
            });
       }
    });
  
});

/**
 * get all uploaded cloths
 */
router.get('/',(req,res,next)=>{
    const limit=6
    const page= req.query.page
    const offset=(page-1)*limit
sql = `SELECT * FROM aladin_gadgets WHERE is_ordered=${1}
LIMIT ${limit} OFFSET ${offset}`;
db.query(sql,(error,result)=>{
    if(error){
        throw error
    }else{
    res.send(result);
}
})
});




router.get('/:id',(req,res)=>{
    if(req.params.id){
        let sql=`SELECT aladin_gadgets.gadg_id,aladin_gadgets.price,aladin_gadgets.dim,aladin_gadgets.is_ordered,aladin_gadgets.staff,aladin_gadgets.user_id,aladin_gadgets.comment,aladin_gadgets.created_at,aladin_gadgets.name_gadget,aladin_gadgets.size,aladin_gadgets.diam,aladin_images.front_side,aladin_images.back_side,aladin_images.customimg,aladin_images.custimg,aladin_texts.text,aladin_texts.fontfamily,aladin_texts.y_value,x_value,aladin_texts.fill,aladin_texts.is_back,aladin_texts.h_y,aladin_texts.w_x,aladin_texts.stroke,aladin_texts.w_stroke FROM aladin_images
        JOIN aladin_gadgets ON aladin_images.gadget=aladin_gadgets.gadg_id 
        JOIN aladin_texts ON aladin_texts.image=aladin_images.id
        WHERE aladin_gadgets.gadg_id=${req.params.id}`;
        db.query(sql,(err,result)=>{
            if(err){
           res.send(err)
            } 
            res.send(result);
        })
    }
})


router.put('/:id',(req,res)=>{
    let sql =`UPDATE aladin_gadgets SET is_ordered=${req.body.is_ordered} WHERE gadg_id=${req.params.id}`
    db.query(sql,(err,result)=>{
      if(err) throw err;
      let newsql=`SELECT user_id FROM aladin_gadgets WHERE gadg_id=${req.params.id}`;
   
     db.query(newsql,(err,re)=>{
         if(err) throw err;
         let ql=`SELECT email FROM aladin_users WHERE user_id=${re[0].user_id}`;
         db.query(ql,(err,r)=>{
             if(err) throw err;
         let message={
        to:r[0].email,
        subject:"Activation de compte Aladin",
        html:"Votre produit a été approuvé."
      };
      sendmail(message.to,message.subject,message.html);
      
            res.status(200).send({
                message:"ous avez approuvé le produit",
                success:true
            });


         })
     });

  
    })
    
  });

module.exports = router;

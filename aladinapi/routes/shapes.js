var express = require('express');
var router = express.Router();
var db = require('../config/db')
var date = require('../config/order_date');
/* Post uploaded file.
 */

router.post('/', function(req, res, next){
    const file = req.files.url

    file.mv("public/images/"+file.name,(err,r)=>{
       if(err){
        throw err;
       }else{
            var sql = "INSERT INTO aladin_shapes(url, name,created_at) VALUES ?";
            var value=[
                ["https://api.aladin.ci/images/"+file.name,req.body.name,date()]
            ];
            db.query(sql,[value],(err,result)=>{
                if(err){
                    res.send(
                        {
                            error:err,
                            status:400,
                            message:"bad request"
                        }
                    )
                    throw err
                }else{
                    res.send(
                        {
                            status:201,
                            data:result,
                            message:"success"
                        }
                    )
                console.log(res);
                }
            });
       }
    });

});

/**
 * get all uploaded shapes
 */
router.get('/',(req,res,next)=>{
    const limit=16
    const page= req.query.page
    const offset=(page-1)*limit
 let sql = `SELECT * FROM aladin_shapes LIMIT ${limit} OFFSET ${offset}`
db.query(sql,(error,result)=>{
    if(error){
        throw error
    }else{
    res.send({status:200,shapes:result});
}
})
});


router.get("/:id",(req,res)=>{
let sql = `SELECT * FROM aladin_shapes WHERE shap_id=${req.params.id}`;
db.query(sql,(err,result)=>{
    if(err) throw err
    console.log(result);
    let r={
        status:200,
        shape:result
    }
    res.send(r);

});

});




module.exports = router;

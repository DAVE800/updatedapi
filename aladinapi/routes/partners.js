var express = require('express');
var router = express.Router();
var db=require('../config/db');


router.get('/', function(req, res, next) {
  db.query("SELECT * FROM aladin_categories",function(err,result){
    if(err) throw err
    res.send(result);
  });
});

router.get('/:id/cloths',(req,res)=>{
  db.query(`SELECT * FROM aladin_cloths WHER user_id=${req.params.id}`,(err,result)=>{
    if(err){
      res.send({
        error:err
      })
    }
    res.send(result)
  })
})

router.get('/:id/packages',(req,res)=>{
db.query(`SELECT * FROM aladin_packaging WHERE user_id=${req.params.id}`,(err,result)=>{
  if(err) throw err;
  res.send(result);
});
})

router.get('/:id/displays',(req,res)=>{
  db.query(`SELECT * FROM aladin_displays WHERE user_id=${req.params.id}`,(err,result)=>{
    if(err) throw err;
    res.send(result);
  });
  })


  router.get('/:id/printed',(req,res)=>{
    db.query(`SELECT * FROM aladin_printed WHERE user_id=${req.params.id}`,(err,result)=>{
      if(err) throw err;
      res.send(result);
    });
    })


  router.get('/:id/gadgets',(req,res)=>{
    db.query(`SELECT * FROM aladin_gadgets WHERE user_id=${req.params.id}`,(err,result)=>{
      if(err) throw err;
      res.send(result);
    });
    })




    router.get('/cloths',(req,res)=>{

      let sql =`SELECT aladin_cloths.img,aladin_cloths.clt_id,aladin_cloths.name_cloths,aladin_cloths.price,aladin_cloths.color,aladin_cloths.mode,aladin_cloths.size,aladin_cloths.material,aladin_cloths.type_imp,aladin_users.name,aladin_cloths.comment,aladin_cloths.is_ordered,aladin_users.email,aladin_users.phone,aladin_users.city,aladin_users.country
       FROM aladin_cloths JOIN aladin_users ON aladin_cloths.user_id=aladin_users.user_id 
      `;
      db.query(sql,(err,result)=>{


        if(err) throw err;
        res.send(result);
      });

    })



    router.get('/displays',(req,res)=>{
      let sql =`SELECT aladin_displays.img,aladin_displays.disp_id,aladin_displays.name_display,aladin_displays.price,aladin_displays.dim,aladin_displays.gram,aladin_users.name,aladin_displays.comment,aladin_displays.is_ordered,aladin_users.email,aladin_users.phone,aladin_users.city,aladin_users.country
       FROM aladin_displays JOIN aladin_users ON aladin_displays.user_id=aladin_users.user_id 
      `;
      db.query(sql,(err,result)=>{


        if(err) throw err;
        res.send(result);
      });

    });


    router.get('/prints',(req,res)=>{
      let sql =`SELECT aladin_printed.img,aladin_printed.print_id,aladin_printed.name_printed,aladin_printed.price,aladin_printed.dim,aladin_printed.gram,aladin_printed.pellicule,aladin_printed.volet,aladin_printed.comment,aladin_printed.is_ordered,aladin_printed.verni,aladin_printed.border,aladin_printed.created_at,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_users.city,aladin_users.country
       FROM aladin_printed JOIN aladin_users ON aladin_users.user_id=aladin_printed.user_id 
      `;
      db.query(sql,(err,result)=>{


        if(err) throw err;
        res.send(result);
      });

    })



    router.get('/gadgets',(req,res)=>{
      let sql =`SELECT aladin_gadgets.img,aladin_gadgets.gadg_id,aladin_gadgets.name_gadget,aladin_gadgets.price,aladin_gadgets.size,aladin_gadgets.diam,aladin_gadgets.material,aladin_gadgets.comment,aladin_gadgets.is_ordered,aladin_gadgets.created_at,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_users.city,aladin_users.country
       FROM aladin_gadgets JOIN aladin_users ON aladin_users.user_id=aladin_gadgets.user_id 
      `;
      db.query(sql,(err,result)=>{
        if(err) throw err;
        res.send(result);
      });

    })



    router.get('/packs',(req,res)=>{
      let sql =`SELECT aladin_packaging.img,aladin_packaging.pack_id,aladin_packaging.name_packaging,aladin_packaging.price,aladin_packaging.dim,aladin_packaging.material,aladin_packaging.comment,aladin_packaging.is_ordered,aladin_packaging.created_at,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_users.city,aladin_users.country
       FROM aladin_packaging JOIN aladin_users ON aladin_users.user_id=aladin_packaging.user_id 
      `;
      db.query(sql,(err,result)=>{


        if(err) throw err;
        res.send(result);
      });

    })



    

module.exports = router;

var express = require('express');
var router = express.Router();
var db = require('../config/db')
var date = require('../config/order_date');
/* Post uploaded file.
 */

router.post('/', function(req, res, next){

            var sql = "INSERT INTO aladin_fonts(url, name,created_at) VALUES ?";
            var value=[
                [req.body.url,req.body.name,date()]
            ];
            db.query(sql,[value],(err,result)=>{
                if(err){
                    res.send(
                        {
                            error:err,
                            status:400,
                            message:"bad request"
                        }
                    )
                    throw err
                }else{
                    res.send(
                        {
                            status:201,
                            data:result,
                            message:"success"
                        }
                    )
                console.log(res);
                }
            });
       
});


/**
 * get all uploaded shapes
 */
router.get('/',(req,res,next)=>{
   
 let sql = `SELECT * FROM aladin_fonts `
db.query(sql,(error,result)=>{
    if(error){
        throw error
    }else{
    res.send({status:200,fonts:result});
}
})
});


router.get("/:id",(req,res)=>{
let sql = `SELECT * FROM aladin_fonts WHERE font_id=${req.params.id}`;
db.query(sql,(err,result)=>{
    if(err) throw err
    console.log(result);
    let r={
        status:200,
        font:result
    }
    res.send(r);

});

});




module.exports = router;
